var express = require('express');
var mysql = require('mysql');
var router = express.Router();

var db_host = 'localhost';
var db_user = 'root';
var db_password = '1qaz2wsx@';
var db_database = 'ihealth4me';
/* GET home page. */
router.get('/', function(req, res) {
		res.render('index.html');
});


router.get('/userCategories', function(req, res) {
	console.log("in userCategories");
    var connection = mysql.createConnection({
     host     : db_host,
     user     : db_user,
     password : db_password,
     database : db_database
    });
	connection.connect();
	var result = null;
	connection.query(
			'select UC_ID as ucID,  UC_NAME as ucName from ihealth4me.USER_CATEGORY ', function(err, rows, fields) {
		if (err) throw err;
		console.log(rows);
		res.send(JSON.stringify(rows));
	});
	connection.end();
});

router.get('/ourMission', function(req, res) {
    res.render('../public/our_mission.html');
});

router.get('/partners', function(req, res) {
    res.render('../public/partners.html');
});

router.get('/team', function(req, res) {
    res.render('../public/team.html');
});

router.get('/contactUs', function(req, res) {
    res.render('../public/contact_us.html');
});


module.exports = router;
