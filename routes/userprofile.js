var express = require('express');
var mysql = require('mysql');
var emailjs   = require('emailjs/email'); 
var ejs = require('ejs');
var fs = require('fs');
var path = require('path');

var db_host = 'localhost';
var db_user = 'root';
var db_password = '1qaz2wsx@';
var db_database = 'ihealth4me';

var server  = emailjs.server.connect({
   user:    "info@iHealth4Me.com", 
   password:"info@iH4M.com", 
   host:    "smtp.ihealth4me.com", 
});

var emailFrom = "info@iHealth4Me.com";

var url = "http://dev.ihealth4me.com";

module.exports = {
  register: function (req, res, callback) {
    var title = req.body.title;
    var firstName = req.body.firstName;
    var lastName = req.body.lastName;
    var dob = req.body.dob;
    var email = req.body.email;
    var password = req.body.password;
    // var userCategory = req.body.userCategory;
    var sex = req.body.sex;
    var country = req.body.country;
    var state = req.body.state;
    var city = req.body.city;
    var postCode = req.body.postCode;

    var uid = getUID();
    var usernameQry = "select USER_ID from ihealth4me.LOGIN where USER_NAME = '"+ email +"';";
    var userProfileQry = "INSERT INTO ihealth4me.USER_PROFILE (TITLE, FIRST_NAME, LAST_NAME, UC_ID, ORGANIZATION_ID, EMAIL_ADDRESS, COUNTRY_ID, STATE, CITY, POST_CODE, DOB, SEX) VALUES ('" + title +"', '"+ firstName  +"', '"+ lastName +"', '"+ 1 +"', '"+ 0 +"', '"+ email +"', '"+ country +"', '"+ state +"', '"+ city +"', '"+ postCode +"', '"+ dob +"', '"+ sex + "');";
    var loginQry = "INSERT INTO ihealth4me.LOGIN (USER_ID, USER_NAME, PASSWORD, ACTIVATED, ACTIVATION_CODE) VALUES ('?', '"+ email +"', '"+ password  +"', '"+ 0 + "', '"+ uid +"' );";
    var email_parameters = {nameParameter: firstName, activationCodeParameter: uid, urlParameter: url};
    var file = fs.readFileSync('views/email_templates/registration.ejs', 'utf8');
    var email_txt = ejs.render(file, email_parameters);
    var connection = mysql.createConnection({
     host     : db_host,
     user     : db_user,
     password : db_password,
     database : db_database
    });
    connection.connect(function(err){console.log(err);});
    
    connection.query(usernameQry, function(err, results) {
        if(err) {
            console.log("Error while selecting from database!");
            callback("error", "Error while selecting from database! ");
            console.log(err);
        } else if (results.length <= 0) {
            connection.query(userProfileQry, function(err, results) {
        if(err) {
            callback("error", "There was (an) error(s) while writing the info into database! ");
            console.log("USER_PROFILE QUERY: " + userProfileQry);
            console.log(err);
        } else {
            console.log("User added to USER_PROFILE table. ");
            var user_id = results.insertId;
            connection.query(loginQry, user_id, function(err, results) {
                if(err) {
                    callback("error", "There was (an) error(s) while writing the info into database! ");
                    console.log("LOGIN QUERY: " + loginQry);
                    console.log(err);
                } else {
                    console.log("Added login info.");
                    callback(null, "You registered successfully! We will email you the activation code shortly. ");
                    // send activation email to the user
                    server.send({
                        text:    email_txt, 
                        from:    emailFrom, 
                        to:      email,
                        cc:      "",
                        subject: "Activate your account",
                        attachment: 
                           [
                              {data: email_txt, alternative:true}
                           ]
                    }, function(err, message) { 
                        if (err) {
                            callback("error", "We could not send you an email! ");
                            console.log(err);
                        } 
                    });
                    connection.end();
                }
            });
        }
            }); 
        } else {
            console.log("There is an account with the provided email address! ");
            callback("error", "There is an account with the provided email address! ");
        }
    }); 
    },

    resetPassword: function(req, res, callback) {
        var username = req.body.username;
        var password = getPassword();

        var updatePassword = "UPDATE ihealth4me.LOGIN SET PASSWORD = '"+ password +"' WHERE LOWER(USER_NAME) LIKE LOWER('"+ username +"');";        var email_parameters = {nameParameter: '', passwordParameter: password, urlParameter: url};
        var email_parameters = {nameParameter: '', passwordParameter: password, urlParameter: url};
        var file = fs.readFileSync('views/email_templates/reset_password.ejs', 'utf8');
        var email_txt = ejs.render(file, email_parameters);
        console.log(email_txt);
        //TODO:  add name to the email (Sebastian)
        var connection = mysql.createConnection({
         host     : db_host,
         user     : db_user,
         password : db_password,
         database : db_database
        });
        connection.connect(function(err){console.log(err);});
        
        connection.query(updatePassword, function(err, results) {
            if(err) {
                callback("error", "Error while updating the database!  ");
                console.log("Error while updating the database!");
                console.log(err);
            } else {
                    numRows = results.affectedRows;
                    if (numRows <= 0){
                        callback("error", "The provided Email address does not exist! ");
                        console.log("The provided Email address does not exist! ");
                    } else {
                        callback(null, "We will email you the new password shortly! ");
                        console.log("We will email you the new password shortly!");
                        server.send({ 
                        text:    email_txt, 
                        from:    emailFrom, 
                        to:      username,
                        cc:      "",
                        subject: "Your password is reset",
                        attachment: 
                           [
                              {data: email_txt, alternative:true}
                           ]
                    }, function(err, message) { 
                        if (err) {
                            callback("error", "We could not send you an email! ");
                            console.log(err);
                        } 
                    });
                    connection.end();
                    }
            }
        }); 
    },

    activate: function(req, res, callback) {
    var usernam = req.body.username;
    var activationCode = req.body.activationCode;

    var updateActivation = "UPDATE ihealth4me.LOGIN SET ACTIVATED = 1 WHERE TRIM(ACTIVATION_CODE) LIKE TRIM('"+ activationCode +"') AND LOWER(USER_NAME) LIKE LOWER('"+ usernam +"');";
    console.log(updateActivation);

    var connection = mysql.createConnection({
     host     : db_host,
     user     : db_user,
     password : db_password,
     database : db_database
    });
    connection.connect(function(err){console.log(err);});
    
    connection.query(updateActivation, function(err, results) {
        if(err) {
            callback("error", "Error while updating the database!  ");
            console.log("Error while updating the database!");
            console.log(err);
        } else {
                numRows = results.affectedRows;
                if (numRows <= 0){
                    callback("error", "The provided info does not math our records ! ");
                    console.log("The provided info does not math our records !");
                } else {
                    callback(null, "Login information is activated successfully! ");
                    console.log("Login info is activated successfully!");
                }
        }
    }); 
    connection.end();
    }
};

getUID = (function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
  }
  return function() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
           s4() + '-' + s4() + s4() + s4();
  };
})();

getPassword = (function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
  }
  return function() {
    return s4() + s4() + s4() + s4();
  };
})();