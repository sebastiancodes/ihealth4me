var express = require('express');
var mysql = require('mysql');
var router = express.Router();

//var authentication = require('../routes/authentication');

router.get('/overview', function(req, res) {
  	res.sendfile('./views/fitness/overview.html');
});

//router.get('/profile', authentication.checkAuth, function(req, res) {
router.get('/profile', function(req, res) {
	res.render('fitness/profile.html');
});

router.get('/healthAssesment', function(req, res) {
	res.render('fitness/healthAssesment.html');
});

router.get('/stats', function(req, res) {
	var userId = req.param('userId');
	console.log("userId = " + userId);
	var connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : '1qaz2wsx@',
	database : 'ihealth4me'
	});
	connection.connect();
	var result = null;
	connection.query('select monthname(timestamp) as month, userIndex as user from ihealth4me.fitness_index where userId = ?', [userId], function(err, rows, fields) {
		if (err) throw err;
		console.log(rows);
		res.send(JSON.stringify(rows));
	});
	connection.end();
});

function sendResultToClient(req, res, data) {
   // res.send("Nothing");
}

router.get('/schedule', function(req, res) {
	var userId = req.param('userId');
	console.log("userId = " + userId);
	var connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : '1qaz2wsx@',
	database : 'ihealth4me'
	});
	connection.connect();
	var result = null;
	connection.query(
			'select a.id, a.title, asch.start, asch.finish as end from ihealth4me.activity_schedules as asch, ihealth4me.activities as a where asch.user_id = ? and a.id = asch.activity_id',
			[userId], function(err, rows, fields) {
		if (err) throw err;
		res.send(JSON.stringify(rows));
	});
	connection.end();
});

router.get('/activities/recommendations', function(req, res) {
	var userId = req.param('userId');
	console.log("userId = " + userId);
	var connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : '1qaz2wsx@',
	database : 'ihealth4me'
	});
	connection.connect();
   var limit = 3;
	var query = 'select a.id, a.title, ar.duration_in_min, ar.days_per_week from ihealth4me.activities as a, ihealth4me.activity_recommendations as ar where ar.user_id = ? and a.id = ar.activity_id limit 0, 3';
	var result = null;
	connection.query(query, [userId], function(err, rows, fields) {
		if (err) throw err;
      console.log(rows);
      var jsonString = "{\"data\":" + JSON.stringify(rows) + "}";
		res.send(jsonString);
	});
	connection.end();
});

router.get('/devices/recommendations', function(req, res) {
   var userId = req.param('userId');
   console.log("userId = " + userId);
   var connection = mysql.createConnection({
      host     : 'localhost',
      user     : 'root',
      password : '1qaz2wsx@',
      database : 'ihealth4me'
   });
   connection.connect();
   var query = 'select d.name, d.url, d.image from ihealth4me.device_recommendations dr, ihealth4me.devices d where dr.user_id = ? and dr.device_id = d.id';
   var result = null;
   connection.query(query, [userId], function(err, rows, fields) {
      if (err) throw err;
      console.log(rows);
      var jsonString = "{\"data\":" + JSON.stringify(rows) + "}";
      res.send(jsonString);
   });
   connection.end();
});

router.post('/saveProfileInfo', function(req, res) {

	//var theUrl = url.parse( req.url );
	//var queryObj = queryString.parse( theUrl.query )
	//var weight = req.param('weight');
	//var profile = JSON.parse( queryObj.jsonData );
	
	//var jsonObject = JSON.parse(req.body);

	var weight =req.body.weight;
	var hgh =req.body.hgh;
	var bmi =req.body.bmi;
	var bodyfat =req.body.bodyfat;
	var bodymass =req.body.bodymass;
	var hrate =req.body.hrate;
	
	console.log(req.body);

});

router.post('/saveHealthAssesmentInfo', function(req, res) {

	//var theUrl = url.parse( req.url );
	//var queryObj = queryString.parse( theUrl.query )
	//var weight = req.param('weight');
	//var profile = JSON.parse( queryObj.jsonData );
	
	//var jsonObject = JSON.parse(req.body);
	//console.log(req);

	var weight =req.body.weight;
	var height =req.body.height;
	var bmi =req.body.bmi;
	var bodyfat =req.body.bodyfat;
	var lbm =req.body.lbm;
	var hrate =req.body.hrate;
	var fbs =req.body.fbs;
	var wm =req.body.wm;
	var hm =req.body.hm;
	var tc =req.body.tc;
	var sbp =req.body.sbp;
	var dbp =req.body.dbp;
	var chol =req.body.chol;
	var hdl =req.body.hdl;
	var ldl =req.body.ldl;

	
	console.log("weight is " + weight);
	console.log("height is " +height);
	console.log("bmi is " +bmi);
	console.log("bodyfat is " +bodyfat);
	console.log("lbm is " +lbm);
	console.log("hrate is " +hrate);
	console.log("fbs is " +fbs);
	console.log("wm is " +wm);
	console.log("hm is " +hm);
	console.log("tc is " +tc);
	console.log("sbp is " +sbp);
	console.log("dbp is " +dbp);
	console.log("chol is " +chol);
	console.log("hdl is " +hdl);
	console.log("ldl is " +ldl);

});

module.exports = router;