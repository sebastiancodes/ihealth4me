var express = require('express');
var mysql = require('mysql');

var db_host = 'localhost';
var db_user = 'root';
var db_password = '1qaz2wsx@';
var db_database = 'ihealth4me';

module.exports = {

	login: function(req, res, callback) {
        var usernam = req.body.username;
        var password = req.body.password;

        var loginStatusQry = "select USER_ID from ihealth4me.LOGIN where USER_NAME = '"+ usernam +"' and  PASSWORD= '"+ password +"' and ACTIVATED = 0;";
        var loginQry = "select USER_ID from ihealth4me.LOGIN where USER_NAME = '"+ usernam +"' and  PASSWORD= '"+ password +"' and ACTIVATED = 1;";
        console.log(loginQry);

        var connection = mysql.createConnection({
         host     : db_host,
         user     : db_user,
         password : db_password,
         database : db_database
        });
        connection.connect(function(err){console.log(err);});
        connection.query(loginStatusQry, function(err, results) {
            if (err) {
                console.log("Error while selecting from database!");
                callback("error", "Error while selecting from database! ");
                console.log(err);
            } else if (results.length <= 0) {
                connection.query(loginQry, function(err, results) {
                    if(err) {
                        console.log("Error while selecting from database!");
                        callback("error", "Error while selecting from database! ");
                        console.log(err);
                    } else if (results.length <= 0) {
                        console.log("Wrong Username and password!");
                        callback("error", "Wrong Username and password! ");
                    } else {
                        req.session.user_id = results[0].USER_ID;
                        console.log("Awesome you loged in successfully!");
                        callback(null, "Awesome you loged in successfully! ");
                    }
                    connection.end();
                }); 
            } else {
                console.log("You need to activate your account to login! ");
                callback("error", "You need to activate your account to login! ");
            }
        });
    },

	checkAuth: function(req, res, next) {
	  if (!req.session.user_id) {
	    req.flash('successMessage', '');
	    req.flash('errorMessage', 'You are not authorized to view this page');
	    res.redirect('/authenticate');
	  } else {
	    next();
	  }
	},

    logout: function (req, res) {
        delete req.session.user_id;   
    }

};