var express = require('express');

var favicon = require('static-favicon');
var logger = require('morgan');

var cookieParser = require('cookie-parser');
//Sebastian 18/06/14 set up the store-based session
var session = require('express-session');
var flash = require('req-flash');

var path = require('path');
var bodyParser = require('body-parser');
var routes = require('./routes/index');
var fitness = require('./routes/fitness');
var userprofile = require('./routes/userprofile');
var authentication = require('./routes/authentication');

var app = express();

var mysql = require('mysql');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('.html', require('ejs').__express);
app.set('view engine', 'ejs');
app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());

//Sebastian 18/06/14 set up the store-based session
app.use(session({ secret: '123' }));
app.use(flash({ locals: 'flash' }));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/fitness', fitness);

app.post('/register', function(req, res) {
    userprofile.register(req, res, function(err, msg) {
        if (err) {
            req.flash('successMessage', '');
            req.flash('errorMessage', msg);
            res.render('index', req.flash());

        } else {
            req.flash('successMessage', msg);
            req.flash('errorMessage', '');
            res.render('activation', req.flash());
        }
    });   
}); 

app.post('/activate', function(req, res) {
    userprofile.activate(req, res, function(err, msg) {
        if (err) {
            req.flash('successMessage', '');
            req.flash('errorMessage', msg);
            res.render('activation', req.flash());

        } else {
            req.flash('successMessage', msg);
            req.flash('errorMessage', '');
            res.render('login', req.flash());
        }
    });   
}); 

app.post('/login', function(req, res) {
    authentication.login(req, res, function(err, msg) {
        if (err) {
            req.flash('successMessage', '');
            req.flash('errorMessage', msg);
            res.render('login', req.flash());

        } else {
            // req.flash('successMessage', msg);
            // req.flash('errorMessage', '');
            // res.render('index', req.flash());
            res.redirect('/fitness/profile');
        }
    });

}); 

app.post('/resetPassword', function(req, res) {
    userprofile.resetPassword(req, res, function(err, msg) {
        if (err) {
            req.flash('successMessage', '');
            req.flash('errorMessage', msg);
            res.render('reset_password', req.flash());

        } else {
            req.flash('successMessage', msg);
            req.flash('errorMessage', '');
            res.render('login', req.flash());
        }
    });  
}); 


app.get('/resetPassword', function(req, res) {
            req.flash('successMessage', '');
            req.flash('errorMessage', '');
            res.render('reset_password', req.flash());
}); 

app.get('/activation', function(req, res) {
            req.flash('successMessage', '');
            req.flash('errorMessage', '');
            res.render('activation', req.flash());
}); 

app.get('/authenticate', function(req, res) {
            res.render('login', req.flash());
}); 

app.get('/logout', function (req, res) {
    authentication.logout(req, res);
    req.flash('successMessage', 'You\'ve been logged out successfully!');
    req.flash('errorMessage', '');
    res.render('login', req.flash());
});     

var port = 8080;
app.listen(port);
console.log("App listening on port " + port);

module.exports = app;
