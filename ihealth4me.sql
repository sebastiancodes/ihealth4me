-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 01, 2014 at 10:34 PM
-- Server version: 5.5.37-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ihealth4me`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `title`, `description`) VALUES
(1, 'Backpacking', NULL),
(2, 'Badminton', NULL),
(3, 'Basketball', NULL),
(4, 'Bicycling', NULL),
(5, 'Billiards', NULL),
(6, 'Bowling', NULL),
(7, 'Canoeing', NULL),
(8, 'Gardening', NULL),
(9, 'Handball', NULL),
(10, 'Jogging', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `activity_recommendations`
--

CREATE TABLE IF NOT EXISTS `activity_recommendations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `is_accepted` bit(1) DEFAULT b'0',
  `duration_in_min` int(11) DEFAULT NULL,
  `days_per_week` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_id` (`user_id`),
  KEY `activity_id` (`activity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `activity_recommendations`
--

INSERT INTO `activity_recommendations` (`id`, `user_id`, `activity_id`, `is_accepted`, `duration_in_min`, `days_per_week`) VALUES
(1, 1, 4, b'0', 30, 3),
(2, 1, 1, b'0', 60, 5),
(3, 1, 5, b'0', 45, 3),
(4, 1, 6, b'0', 30, 4),
(5, 1, 3, b'0', 45, 4);

-- --------------------------------------------------------

--
-- Table structure for table `activity_schedules`
--

CREATE TABLE IF NOT EXISTS `activity_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `activity_id` int(11) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `finish` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `user_id` (`user_id`),
  KEY `activity_id` (`activity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `activity_schedules`
--

INSERT INTO `activity_schedules` (`id`, `user_id`, `activity_id`, `start`, `finish`) VALUES
(1, 1, 4, '2014-05-19 07:00:00', '2014-05-19 07:30:00'),
(2, 1, 2, '2014-05-20 06:00:00', '2014-05-20 08:00:00'),
(3, 1, 3, '2014-05-19 06:00:00', '2014-05-19 06:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `devices`
--

CREATE TABLE IF NOT EXISTS `devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `url` text,
  `image` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `devices`
--

INSERT INTO `devices` (`id`, `name`, `description`, `url`, `image`) VALUES
(1, 'FitBit', NULL, 'http://www.fitbit.com/au', 'fitbit.jpg'),
(2, 'Conair', NULL, NULL, 'conair.png'),
(3, 'InBody', NULL, NULL, 'in-body.jpg'),
(4, 'Nike+ FuelBand', NULL, 'http://www.nike.com/us/en_us/c/nikeplus-fuelband', 'nike-fuel-band.jpg'),
(5, 'Jawbone UP', NULL, 'https://jawbone.com/up', 'jawbone-up.jpg'),
(6, 'Speed Cell', NULL, NULL, 'speed-cell.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `device_recommendations`
--

CREATE TABLE IF NOT EXISTS `device_recommendations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `device_id` (`device_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `device_recommendations`
--

INSERT INTO `device_recommendations` (`id`, `user_id`, `device_id`) VALUES
(1, 1, 1),
(2, 1, 4),
(3, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `fitness_index`
--

CREATE TABLE IF NOT EXISTS `fitness_index` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `userIndex` int(11) DEFAULT NULL,
  `peerGroupIndex` int(11) DEFAULT NULL,
  `timestamp` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `fitness_index`
--

INSERT INTO `fitness_index` (`id`, `userId`, `userIndex`, `peerGroupIndex`, `timestamp`) VALUES
(1, 1, 70, 65, '2014-05-10 15:08:43'),
(2, 1, 81, 67, '2014-06-11 15:08:41'),
(3, 1, 75, 68, '2014-07-11 15:08:45'),
(4, 1, 78, 75, '2014-08-11 15:08:32'),
(5, 1, 62, 71, '2014-09-11 15:09:32');

-- --------------------------------------------------------

--
-- Table structure for table `peer_groups`
--

CREATE TABLE IF NOT EXISTS `peer_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `peer_groups`
--

INSERT INTO `peer_groups` (`id`, `name`) VALUES
(1, 'Students (18-25)'),
(2, 'Athletes'),
(3, 'Teenagers'),
(4, 'Seniors'),
(5, 'Adults (30-40)'),
(6, 'Adults (18-30)'),
(7, 'Students (25-35)');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `firstname` text,
  `lastname` text,
  `peer_group` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `peerGroup` (`peer_group`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `firstname`, `lastname`, `peer_group`) VALUES
(1, 'samvitA@ihealth4me.com', 'password', 'Samvit', 'Acharya', 1),
(2, 'agamM@ihealth4me.com', 'password', 'Agam', 'Misra', 1),
(3, 'arunK@ihealth4me.com', 'password', 'Arunmainthan', 'Kamalanathan', 7),
(4, 'sebastianS@ihealth4me.com', 'password', 'Sebastian', 'Saeidi', 1),
(5, 'peterK@ihealth4me.com', 'password', 'Peter', 'Klement', NULL),
(6, 'bodoW@ihealth4me.com', 'password', 'Bodo', 'Wiegand', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activity_recommendations`
--
ALTER TABLE `activity_recommendations`
  ADD CONSTRAINT `activity_recommendations_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `activity_recommendations_ibfk_2` FOREIGN KEY (`activity_id`) REFERENCES `activities` (`id`);

--
-- Constraints for table `activity_schedules`
--
ALTER TABLE `activity_schedules`
  ADD CONSTRAINT `activity_schedules_ibfk_2` FOREIGN KEY (`activity_id`) REFERENCES `activities` (`id`),
  ADD CONSTRAINT `activity_schedules_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `device_recommendations`
--
ALTER TABLE `device_recommendations`
  ADD CONSTRAINT `device_recommendations_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `device_recommendations_ibfk_2` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_peer_groups` FOREIGN KEY (`peer_group`) REFERENCES `peer_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
