select * from ihealth4me.users;

select * from ihealth4me.activities;

-- get activity recommendations for a user
select a.id, a.title
from ihealth4me.activities as a, ihealth4me.activity_recommendations as ar
where ar.user_id = 1 and a.id = ar.activity_id;

-- get activities scheduled by a user
select a.id, a.title, asch.start, asch.finish
from ihealth4me.activity_schedules as asch, ihealth4me.activities as a
where asch.user_id = 1 and a.id = asch.activity_id;

select u.firstname, pg.name
from users u, peer_groups pg
where u.peerGroup = pg.id;

-- user's most recent index
-- user's best index
-- user's peer group's most recent index
-- user's peer group's best index
select * from fitness_index;
select max(userIndex), max(peerGroupIndex)
from fitness_index where userId = 1;
select userIndex, peerGroupIndex, timestamp
from fitness_index where userId = 1 and timestamp = (select max(timestamp) from fitness_index where userId = 1);























